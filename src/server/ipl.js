const papa = require('papaparse')       // papaparse is loaded in papa
const fileSystem = require('fs');       // used to read and write a file
const path = require('path');



function csvToJson(filePath) {                                                              // csvToJson will convert csv file to json object using papaparse

    if (filePath === undefined || typeof filePath !== 'string') {
        return null;
    }
    else {
        const myPath = path.resolve(__dirname, filePath);
        const file = fileSystem.readFileSync(myPath, 'utf-8');

        const config = {
            header: true,
            dynamicTyping: true
        };

        return papa.parse(file, config).data;
    }

}


const deliveries = csvToJson('../data/deliveries.csv');     // stored deliveries as an object 
const matches = csvToJson('../data/matches.csv');           // stored matches as an object



function matchesPlayedPerYear(matches) {

    let matchesPlayed = matches.filter((match) => {
        return match['id'] !== null;
    })
        .reduce((matchesPerYear, match) => {
            if (matchesPerYear[match['season']] === undefined) {
                matchesPerYear[match['season']] = 1;
            } else {
                matchesPerYear[match['season']]++;
            }

            return matchesPerYear;
        }, {});

    return matchesPlayed;

}



function matchesWonPerTeamPerYear(matches) {

    let wonPerTeam = matches.filter((match) => {
        return match['id'] !== null && match['winner'] !== null;
    })
        .reduce((wonPerTeamPerYear, match) => {
            if (wonPerTeamPerYear[match['season']] === undefined) {
                wonPerTeamPerYear[match['season']] = {};
            }

            if (wonPerTeamPerYear[match['season']][match['winner']] === undefined) {
                wonPerTeamPerYear[match['season']][match['winner']] = 1;
            } else {
                wonPerTeamPerYear[match['season']][match['winner']]++;
            }

            return wonPerTeamPerYear;
        }, {});

    return wonPerTeam;

}



function extraRunsPerTeam2016(matches, deliveries) {

    let extra = deliveries.filter((deliverie) => {
        let matchIdEqualsId = matches.some((match) => {
            return match['season'] === 2016 && match['id'] === deliverie['match_id']
        });

        return matchIdEqualsId === true;
    })
        .reduce((extraRuns, ball) => {
            if (extraRuns[ball['bowling_team']] === undefined) {
                extraRuns[ball['bowling_team']] = ball['extra_runs'];
            } else {
                extraRuns[ball['bowling_team']] += ball['extra_runs'];
            }
            return extraRuns;
        }, {});

    return extra;

}



function top10EconomicalBowlers2015(matches, deliveries) {

    let bowlers = deliveries.filter((deliverie) => {
        let matchIdEqualsId = matches.some((match) => {
            return match['season'] === 2015 && match['id'] === deliverie['match_id'];
        });

        return matchIdEqualsId === true;
    })
        .reduce((economicalBowlers, ball) => {
            if (economicalBowlers[ball['bowler']] === undefined) {
                economicalBowlers[ball['bowler']] = {};
            }

            if (economicalBowlers[ball['bowler']]['balls'] === undefined) {
                economicalBowlers[ball['bowler']]['balls'] = 0;
            }

            if (economicalBowlers[ball['bowler']]['runs'] === undefined) {
                economicalBowlers[ball['bowler']]['runs'] = 0;
            }

            if (economicalBowlers[ball['bowler']]['economy_rate'] === undefined) {
                economicalBowlers[ball['bowler']]['economy_rate'] = 0;
            }

            economicalBowlers[ball['bowler']]['balls']++;

            /*
                    wide and noball will not be included in balls
                    where as runs will be counted and effect bowlers economy
                    bye and leg_bye runs will not effect bowlers economy
 
                    economy is calculated as runs_given_by_bowler / (balls/6) and then calculated up to two decimal places using Math.round()
            */

            economicalBowlers[ball['bowler']]['runs'] += ball['total_runs'] - ball['bye_runs'] - ball['legbye_runs'];

            let economy = economicalBowlers[ball['bowler']]['runs'] / (economicalBowlers[ball['bowler']]['balls'] / 6);

            economicalBowlers[ball['bowler']]['economy_rate'] = parseFloat(economy.toFixed(2));

            return economicalBowlers;
        }, {});

    let arr = Object.entries(bowlers)
        .sort(([, bowler1], [, bowler2]) => {
            return bowler1['economy_rate'] - bowler2['economy_rate'];
        })
        .slice(0, 10);

    return Object.fromEntries(arr);
    /*
    here Object.entries will convert object to arrays which can be easily sorted, using sort()
    and to take out the top 10 we can use slice(0,10) which will return first 10 results
    Objects.fromEntries() will convert arrays to object
    */
}


function wonTossAndMatch(matches) {

    let wonBoth = matches.filter((match) => {
        return match['toss_winner'] === match['winner'] && match['id'] !== null;
    })
        .reduce((acc, curr) => {
            acc[curr['toss_winner']] = acc[curr['toss_winner']] + 1 || 1;
            return acc;
        }, {});

    return wonBoth;

}


function highestPlayerOfTheMatchPerSeason(matches) {

    let playersOfTheMatch = {};

    matches.filter((match) => {
        return match['id'] !== null && match['winner'] !== null;
    })
        .reduce((acc, curr) => {
            if (acc[curr['season']] === undefined) {
                acc[curr['season']] = {};
            }

            if (acc[curr['season']][curr['player_of_match']] === undefined) {
                acc[curr['season']][curr['player_of_match']] = 0;
            }

            acc[curr['season']][curr['player_of_match']]++;

            let arr = Object.entries(acc[curr['season']])
                .sort(([, player1], [, player2]) => {
                    return player2 - player1;
                })
                .slice(0, 1);

            playersOfTheMatch[curr['season']] = Object.fromEntries(arr);

            return acc;
        }, {});

    return playersOfTheMatch;

}


function strikeRate(matches, deliveries) {

    let strike = deliveries.filter((player) => {
        return player['batsman'] === 'MS Dhoni';
    })
        .reduce((acc, curr) => {
            if (acc[curr['batsman']] === undefined) {
                acc[curr['batsman']] = {};
            }

            let season = matches.filter((match) => {
                return match['id'] === curr['match_id'];
            })[0]['season'];

            if (acc[curr['batsman']][season] === undefined) {
                acc[curr['batsman']][season] = { balls: 0, runs: 0, strick_rate: 0 };
            }

            if (curr['wide_runs'] === 0) {
                acc[curr['batsman']][season]['balls']++;
                acc[curr['batsman']][season]['runs'] += curr['batsman_runs'];

                let strick_rate = acc[curr['batsman']][season]['runs'] / acc[curr['batsman']][season]['balls'] * 100;

                acc[curr['batsman']][season]['strick_rate'] = parseFloat(strick_rate.toFixed(2));
            }

            return acc;
        }, {});

    return strike;
}



function highestDismissalByOneToOnePlayer(deliveries) {

    let player = { batsman: null, bowler: null, dismissal: 0 };

    deliveries.filter((ball) => {
        return ball['dismissal_kind'] !== 'run out' && ball['dismissal_kind'] !== null;
    })
        .reduce((acc, curr) => {
            if (acc[curr['batsman']] === undefined) {
                acc[curr['batsman']] = {};
            }

            if (acc[curr['batsman']][curr['bowler']] === undefined) {
                acc[curr['batsman']][curr['bowler']] = 0;
            }

            acc[curr['batsman']][curr['bowler']]++;

            if (player['dismissal'] < acc[curr['batsman']][curr['bowler']]) {
                player['batsman'] = curr['batsman'];
                player['bowler'] = curr['bowler'];
                player['dismissal'] = acc[curr['batsman']][curr['bowler']];
            }
            return acc;
        }, {});

    return player;
}


function bestBowler(deliveries) {

    let player = deliveries.filter((ball) => {
        return ball['is_super_over'] !== 0 && ball['match_id'] !== null;
    })
        .reduce((acc, curr) => {

            if (acc[curr['bowler']] === undefined) {
                acc[curr['bowler']] = { balls: 0, runs: 0, economy_rate: 0 };
            }

            acc[curr['bowler']]['balls']++;
            acc[curr['bowler']]['runs'] += curr['total_runs'] - curr['bye_runs'] - curr['legbye_runs'];

            let economy_rate = acc[curr['bowler']]['runs'] / (acc[curr['bowler']]['balls'] / 6)

            acc[curr['bowler']]['economy_rate'] = parseFloat(economy_rate.toFixed(2));

            return acc;
        }, {});

    let bestPlayer = Object.entries(player)
        .sort(([, a], [, b]) => {
            return a.economy_rate - b.economy_rate;
        })
        .slice(0, 1);

    return Object.fromEntries(bestPlayer);
}

function myPath(filePath) {
    return path.resolve(__dirname, filePath);
}


fileSystem.writeFileSync(myPath('../public/output/matchesPlayedPerYear.json'), JSON.stringify(matchesPlayedPerYear(matches)));
fileSystem.writeFileSync(myPath('../public/output/matchesWonPerTeamPerYear.json'), JSON.stringify(matchesWonPerTeamPerYear(matches)));
fileSystem.writeFileSync(myPath('../public/output/extraRunsPerTeam2016.json'), JSON.stringify(extraRunsPerTeam2016(matches, deliveries)));
fileSystem.writeFileSync(myPath('../public/output/top10EconomicalBowlers2015.json'), JSON.stringify(top10EconomicalBowlers2015(matches, deliveries)));
fileSystem.writeFileSync(myPath('../public/output/teamsWonTossAndMatches.json'), JSON.stringify(wonTossAndMatch(matches)));
fileSystem.writeFileSync(myPath('../public/output/highestPlayerOfTheMatchPerSeason.json'), JSON.stringify(highestPlayerOfTheMatchPerSeason(matches)));
fileSystem.writeFileSync(myPath('../public/output/strikeRate.json'), JSON.stringify(strikeRate(matches, deliveries)));
fileSystem.writeFileSync(myPath('../public/output/highestDismissalByOneToOnePlayer.json'), JSON.stringify(highestDismissalByOneToOnePlayer(deliveries)));
fileSystem.writeFileSync(myPath('../public/output/bestBowler.json'), JSON.stringify(bestBowler(deliveries)));